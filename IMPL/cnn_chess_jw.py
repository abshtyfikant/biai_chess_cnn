import tkinter as tk
from tkinter import filedialog
from PIL import Image, ImageTk
import cv2
from keras.models import load_model

import math
import cv2
import numpy as np
import scipy.spatial as spatial
import scipy.cluster as cluster
from collections import defaultdict
from statistics import mean
import chess
import chess.svg
from svglib.svglib import svg2rlg
from reportlab.graphics import renderPM
from PIL import Image
import re
import glob
import PIL
from cv_chess_functions import (convert_image_to_bgr_numpy_array,
                                prepare_image)

def fen_to_chess_piece(fen_char):
    chess_pieces = {
        'p': 'Black Pawn',
        'r': 'Black Rook',
        'n': 'Black Knight',
        'b': 'Black Bishop',
        'q': 'Black Queen',
        'k': 'Black King',
        'P': 'White Pawn',
        'R': 'White Rook',
        'N': 'White Knight',
        'B': 'White Bishop',
        'Q': 'White Queen',
        'K': 'White King',
    }

    return chess_pieces.get(fen_char, 'Empty Square')
    
def classify_cropped_img(model, filename):
    category_reference = {0: 'b', 1: 'k', 2: 'n', 3: 'p', 4: 'q', 5: 'r', 6: '1', 7: 'B', 8: 'K', 9: 'N', 10: 'P', 11: 'Q', 12: 'R'}
    img = prepare_image(filename)
    print('A')
    out = model.predict(img)
    top_pred = np.argmax(out)
    print('1')
    pred = category_reference[top_pred]
    print('2')
    return pred

def open_image():
    file_path = filedialog.askopenfilename()
    if file_path:

        image = cv2.imread(file_path)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)  # Convert from BGR to RGB

        pil_image = Image.fromarray(image)
         
        # Oryginalne rozmiary obrazu
        width, height = pil_image.size
        
        # Dopasowanie obrazu do szerokości okna, zachowując proporcje
        new_height = 350
        new_width = int((new_height / height) * width)
        pil_image = pil_image.resize((new_width, new_height))

        photo = ImageTk.PhotoImage(pil_image)
        label.config(image=photo)
        label.image = photo

        # Aktualizacja przewijania
        canvas.config(scrollregion=canvas.bbox("all"))
        
        model = load_model('model2.h5')
        settext = fen_to_chess_piece(classify_cropped_img(model, file_path))
        label_text.config(text=f"Result: {settext}")


# Tworzenie głównego okna
root = tk.Tk()
root.title("Chess pieces classification")

# Ustawienie stałych wymiarów okna
root.geometry("600x400")
root.resizable(width=False, height=False)

# Tworzenie ramki z przewijaniem
frame = tk.Frame(root)
frame.pack(fill=tk.BOTH, expand=True)

# Przycisk do otwierania zdjęcia
open_button = tk.Button(frame, text="Choose a file", command=open_image)
open_button.pack()

# Tworzenie przewijanej ramki dla obrazu
canvas = tk.Canvas(frame)
canvas.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)

# Dodawanie przewijania
scrollbar = tk.Scrollbar(frame, command=canvas.yview)
scrollbar.pack(side=tk.RIGHT, fill=tk.Y)
canvas.config(yscrollcommand=scrollbar.set)

# Etykieta na podgląd zdjęcia
label = tk.Label(canvas)
label.pack()

# Dodanie opisu pod podglądem zdjęcia
label_text = tk.Label(frame, text="Result: N/A")
label_text.pack()

# Rozpoczęcie pętli głównej programu
root.mainloop()
