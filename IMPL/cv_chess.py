import re
import cv2
from keras.models import load_model
from cv_chess_functions import (read_img,
                               canny_edge,
                               hough_line,
                               h_v_lines,
                               line_intersections,
                               cluster_points,
                               augment_points,
                               write_crop_images,
                               grab_cell_files,
                               #classify_cells
                               classify_cells_2,
                               fen_to_image,
                               atoi)
from fentoimage.board import BoardImage


# Resize the frame by scale by dimensions
def rescale_frame(frame, percent=75):
    width = int(frame.shape[1] * (percent / 100))
    height = int(frame.shape[0] * (percent / 100))
    # dim = (1000, 750)
    dim = (width, height)
    return cv2.resize(frame, dim, interpolation=cv2.INTER_AREA)


# Find the number(s) in the text
def natural_keys(text):
    return [atoi(c) for c in re.split('(\d+)', text)]


# Load in the CNN model
model = load_model('model1.h5')

# Load the image from the file path
image_path = r".\IMPL\pRt60BOT.jpeg"
frame = cv2.imread(image_path)

# Show the starting board either as blank or with the initial setup
# start = 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR'
blank = '8/8/8/8/8/8/8/8'
board = fen_to_image(blank)
board_image = cv2.imread('current_board.png')
cv2.imshow('current board', board_image)

# Resizes each frame
small_frame = rescale_frame(frame)

print('Working...')
# Save the frame to be analyzed
cv2.imwrite('frame.jpeg', frame)

# Low-level CV techniques (grayscale & blur)
img, gray_blur = read_img('frame.jpeg')

# Canny algorithm
edges = canny_edge(gray_blur)

# Hough Transform
lines = hough_line(edges)

# Separate the lines into vertical and horizontal lines
h_lines, v_lines = h_v_lines(lines)

# Find and cluster the intersecting
intersection_points = line_intersections(h_lines, v_lines)
points = cluster_points(intersection_points)

# Final coordinates of the board
points = augment_points(points)
print(points)

# Crop the squares of the board a organize into a sorted list
x_list = write_crop_images(img, points, 0)

img_filename_list = grab_cell_files()
img_filename_list.sort(key=natural_keys)

# Classify each square and output the board in Forsyth-Edwards Notation (FEN)
#fen = classify_cells(model, img_filename_list)
fen = classify_cells_2(model, img_filename_list)

# Create and save the board image from the FEN
board = fen_to_image(fen)
# Display the board in ASCII
print(board)
print(fen)
# Display and save the board image
renderer = BoardImage(fen)
image = renderer.render()
image.show()

#cv2.imshow("current board", board)
#cv2.waitKey(0)
#cv2.destroyAllWindows()